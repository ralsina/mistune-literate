import ast
import io
import pprint
import re
import sys
import traceback

import mistune
from pygments import highlight
from pygments.formatters import html
from pygments.lexers import get_lexer_by_name
from pygments.util import ClassNotFound

REAL_PRINT = print
REAL_PPRINT = pprint.pprint


def parse_marker(marker):
    """Given a marker of the form

    language|key1=value,key2=value

    parse it and return as:

    lexer, options

    where lexer is a valid pygments lexer and options is a dictionary of keys and values.
    """
    if marker is None:
        marker = "text"

    lang, *options = marker.split("|", 1)
    if options == []:
        options = {}
    else:
        options = options[0]
        options = [o.split("=", 1) for o in options.split(",")]
        options = {k.strip(): ast.literal_eval(v) for k, v in options}
    lang = lang.strip().lower()
    try:
        lexer = get_lexer_by_name(lang)
    except ClassNotFound:
        lexer = get_lexer_by_name("text")
    return lexer, options


collected_code = []


def exec_source(path, source, context, raise_exceptions=False):
    output = io.StringIO()
    # logging_handler = logging.StreamHandler(stream=output)

    def my_print(*args, **kwargs):
        kwargs["file"] = output
        REAL_PRINT(*args, **kwargs)

    def my_pprint(*args, **kwargs):
        kwargs["stream"] = output
        kwargs["width"] = 65  # Max width of monospace code lines in Word
        REAL_PPRINT(*args, **kwargs)

    def my_debug(*args, **kwargs):
        kwargs["file"] = sys.stderr
        REAL_PRINT(*args, **kwargs)

    def my_help(*args, **kwargs):
        helper = pydoc.Helper(output=output)
        return helper(*args, **kwargs)

    def my_pdb():
        # Clear any alarm clocks since we're going interactive.
        signal.alarm(0)

        p = pdb.Pdb(stdin=sys.stdin, stdout=sys.stderr)
        p.use_rawinput = True
        return p

    context["print"] = my_print
    context["pprint"] = my_pprint
    context["debug"] = my_debug
    context["help"] = my_help
    context["Pdb"] = my_pdb
    context["STDOUT"] = output
    # logging.getLogger().addHandler(logging_handler)
    try:
        node = ast.parse(source, path)
        code = compile(node, path, "exec")
        exec(code, context, context)
    except Exception as e:
        # Restore the print functions for other code in this module.
        context["print"] = REAL_PRINT

        # If there's been any output so far, print it out before the
        # error traceback is printed so we have context for debugging.
        output_so_far = output.getvalue()
        if output_so_far:
            print(output_so_far, end="", file=sys.stderr)

        if raise_exceptions:
            raise WrappedException(output_so_far) from e

        format_list = traceback.format_exception(*sys.exc_info())

        # Only show tracebacks for code that was in the target source file.
        # This way calls to parse(), compile(), and exec() are removed.
        cutoff = 0
        for cutoff, line in enumerate(format_list):
            if path in line:
                break

        formatted = "".join(format_list[cutoff:])
        print("Traceback (most recent call last):", file=sys.stderr)
        print(formatted, end="", file=sys.stderr)
        raise Exception(e)
    finally:
        # Restore the print function for other code in this module.
        context["print"] = REAL_PRINT
        # Disable the logging handler.
        # logging.getLogger().removeHandler(logging_handler)

    return output.getvalue()


# The original mistune grammar doesn't consider "~~~~python whatever" a fence
# because it stops on whitespace. I want to use fences to pass along parameters
# so, changing that.
mistune.BlockGrammar.fences = re.compile(
    r"^ *(`{3,}|~{3,}) *([^`\n]+)? *\n([\s\S]+?)\s*\1 *(?:\n+|$)"
)


class HighlightRenderer(mistune.Renderer):
    def block_code(self, code, lang):
        lexer, options = parse_marker(lang)
        if lexer.name == "Text only" and code.strip() in ["", "OUTPUT"]:
            output = exec_source(".", "\n".join(collected_code), {})
            return f"<pre>{output}</pre>"
        elif lexer.name == "Python":
            collected_code.append(code)

        formatter = html.HtmlFormatter()
        return highlight(code, lexer, formatter)


if __name__ == "__main__":
    renderer = HighlightRenderer()
    markdown = mistune.Markdown(renderer=renderer)
    print(
        markdown(
            """
```python | coso='foo'
foo = 124
```

Bla bla

```python
print(f'{foo} bar')
```

foo foo

```

```
"""
        )
    )
