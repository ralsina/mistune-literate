# What?

The goal of this experimental project is to create something similar to [PyLiterate](https://github.com/bslatkin/pyliterate)
but based on [Mistune](https://github.com/lepture/mistune) and implemented as proper extensions and not as a preprocessor.

The basic idea is that it's a Markdown document where you can write python code and it will be executed on "build time"
thus allowing some extent of literate programming.

For example:

    This would be normal text. And now I will add an executable python block:

    ```python
    foo = 124
    ```

    Followed by some text, and then more executable python:

    ```python
    print(f'{foo} bar')
    ```

    And then a block where the output of the previous code will be inserted:

    ```

    ```

What this should output when processed (with nicer syntax highlighting) is:

This would be normal text. And now I will add an executable python block:

<div class="highlight"><pre><span></span><span class="n">foo</span> <span class="o">=</span> <span class="mi">124</span>
</pre></div>
<p>Followed by some text, and then more executable python:</p>
<div class="highlight"><pre><span></span><span class="k">print</span><span class="p">(</span><span class="n">f</span><span class="s1">&#39;{foo} bar&#39;</span><span class="p">)</span>
</pre></div>
<p>And then a block where the output of the previous code will be inserted:</p>
<pre>124 bar
</pre>

# Current Status

Sort of works sometimes for some things.